package seLAB4;

import ie.ucd.items.AlcoholicDrink;
import ie.ucd.items.Drink;
import ie.ucd.items.Food;
import ie.ucd.people.Person;

public class Drinker extends Person{
    public int numberOfDrinks = 0;

    /*
	 * This is a method to set person's weight. 
	*/
	public Drinker(double weight) {
		super();
		this.setWeight(weight); 
	}
	
	/*
	 * This is a method to count how much alcohol drinks does a person drunk.
	*/
	public boolean drink(Drink drink) {
		if(drink instanceof AlcoholicDrink) {
			numberOfDrinks = numberOfDrinks + 1;
		}
		return true;
	}

	@Override
	public boolean eat(Food food) {
		return false;
	}
	
	/* This is a method to judge a person is drunk or not.
	 * If drunk more than 1/10 weight alcoholic drinks, then the person is drunk.
	*/
	public boolean isDrunk() {
		return this.numberOfDrinks > getWeight()/10;	
	}

}
