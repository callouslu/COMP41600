package seLAB4;

import ie.ucd.items.AlcoholicDrink;
import ie.ucd.items.Drink;
import ie.ucd.items.Food;
import ie.ucd.items.NotAlcoholicDrink;
import ie.ucd.people.Person;

public class NotDrinker extends Person{

	/* Judge whether a NotDrinker grab alcohol or not.
	* If it is alcohol drink, then he is not allowed to drink and return false.
	* Or he is allowed to drink the alcohol-free drink and system return true.
	*/
	@Override
	public boolean drink(Drink drink) {
		if(drink instanceof AlcoholicDrink) {
			System.out.println("Go away! You can't drink this!");
			return false;
		}
		if (drink instanceof NotAlcoholicDrink) {
			System.out.println("Go ahead!");	
		}
		return true;
	}

	@Override
	public boolean eat(Food food) {
		return false;
	}

}
