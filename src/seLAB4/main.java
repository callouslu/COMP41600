/*Changkai Lu
**Student number: 12257202
*/

package seLAB4;

import java.util.Scanner;

import ie.ucd.items.Wine;
import ie.ucd.people.Person;

public class main {
	public static void main(String args[]) {
		/*
		 * Initialize drinker and drink
		 */
		Drinker drinker = new Drinker(35);
		NotDrinker notDrinker = new NotDrinker();
		Wine wine = new Wine("erguotou", 50, 40, null);

		/*
		 * Test correct implementation of class Drinker
		 */
		boolean a = drinker.drink(wine);
		System.out.println("Is drinker allowed to drink alcholic Drink? " + a + "\n\n\n");

		/*
		 * Test correct implementation of class NotDrinker
		 */
		boolean b = notDrinker.drink(wine);
		System.out.println("Is notDrinker allowed to drink alcholic Drink? " + b + "\n\n\n");

		/* Test method isDrunk. Ask user whether want to drink one more shot
		 * or not, if say yes(Y), then judge whether the person is drunk. If say
		 * no(N), then break the program.
		 */
		String Y ="Y";
		String N ="N";
		for(int i=1; i<11; i=i+1) {
			Scanner sc = new Scanner(System.in);
			System.out.print("Drink one more or not? (Y/N)  ");
			String userinput = sc.next();
			if(userinput.equals(Y)) {
				drinker.drink(wine);
				boolean c = drinker.isDrunk();
				System.out.println("Is drinker get drunk? " + c);
			}
			else {
				System.out.print("OK, BYE~ ");
				break;
			}
		}
	}
}
